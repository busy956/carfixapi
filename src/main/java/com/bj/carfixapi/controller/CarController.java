package com.bj.carfixapi.controller;

import com.bj.carfixapi.model.CarItem;
import com.bj.carfixapi.model.CarRequest;
import com.bj.carfixapi.model.CarResponse;
import com.bj.carfixapi.model.CarTypeChangeRequest;
import com.bj.carfixapi.repository.CarRepository;
import com.bj.carfixapi.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    @PostMapping("/repair")
    public String setCar(@RequestBody CarRequest request) {
        carService.setCar(request);

        return "Ok";
    }

    @GetMapping("/all")
    public List<CarItem> getCars() {
        return carService.getCars();
    }

    @GetMapping("/detail/{id}")
    public CarResponse getCar(@PathVariable long id) {
        return carService.getCar(id);
    }

    @PutMapping("/type/{id}")
    public String putCarType(@PathVariable long id, @RequestBody CarTypeChangeRequest request) {
        carService.putCarType(id, request);

        return "OK";
    }
}
