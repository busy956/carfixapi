package com.bj.carfixapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate fixDate;

    @Column(nullable = false, length = 20)
    private String fixerName;

    @Column(nullable = false, length = 20)
    private String carType;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false, length = 40)
    private String fixContent;

    @Column(columnDefinition = "TEXT")
    private String ectMemo;
}
