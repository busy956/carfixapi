package com.bj.carfixapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarChange {
    private LocalDate fixDate;
    private String fixerName;
    private String carType;
    private Double price;
    private String fixContent;
    private String ectMemo;
}
