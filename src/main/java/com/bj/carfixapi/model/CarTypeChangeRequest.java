package com.bj.carfixapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarTypeChangeRequest {
    private String carType;
}
