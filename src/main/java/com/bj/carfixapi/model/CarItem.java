package com.bj.carfixapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarItem {
    private Long id;
    private LocalDate fixDate;
    private String fixerName;
    private String carType;
}
