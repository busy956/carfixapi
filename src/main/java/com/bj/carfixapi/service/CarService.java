package com.bj.carfixapi.service;

import com.bj.carfixapi.entity.Car;
import com.bj.carfixapi.model.*;
import com.bj.carfixapi.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;

    public void setCar(CarRequest request) {
        Car addData = new Car();
        addData.setFixDate(request.getFixDate());
        addData.setFixerName(request.getFixerName());
        addData.setCarType(request.getCarType());
        addData.setPrice(request.getPrice());
        addData.setFixContent(request.getFixContent());
        addData.setEctMemo(request.getEctMemo());

        carRepository.save(addData);
    }

    public List<CarItem> getCars() {
        List<Car> originData = carRepository.findAll();

        List<CarItem> result = new LinkedList<>();

        for (Car car : originData) {
            CarItem addItem = new CarItem();
            addItem.setId(car.getId());
            addItem.setFixDate(car.getFixDate());
            addItem.setFixerName(car.getFixerName());
            addItem.setCarType(car.getCarType());

            result.add(addItem);
        }
        return result;
    }

    public CarResponse getCar(long id) {
        Car originData = carRepository.findById(id).orElseThrow();

        CarResponse response = new CarResponse();
        response.setId(originData.getId());
        response.setFixDate(originData.getFixDate());
        response.setFixerName(originData.getFixerName());
        response.setCarType(originData.getCarType());
        response.setPrice(originData.getPrice());
        response.setFixContent(originData.getFixContent());
        response.setEctMemo(originData.getEctMemo());

        return response;
    }
    public void putCarType(long id, CarTypeChangeRequest request) {
        Car originData = carRepository.findById(id).orElseThrow();
        originData.setCarType(request.getCarType());

        carRepository.save(originData);
    }
}

