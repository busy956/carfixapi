package com.bj.carfixapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarFixApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarFixApiApplication.class, args);
	}

}
